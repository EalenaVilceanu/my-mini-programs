print('Welcome to my computer quiz!')
playing = input('Do you want to play? ')
if playing.lower() != 'yes':
    quit()
print("ok! Let's play...")
score = 0

answer = input('What is the logical continuation of the string of numbers 1, 2, 4, 8, … ?  ')
if answer.lower() == '16' or 'sixteen':
    print('Correct!')
    score += 1
else:
    print('Incorrect!')

answer = input('Which is the 50th number of this sequence: 5, 11, 17, 23, 29, 35, 41, …? ')
if answer.lower() == '299' or 'tow hundreed ninety-nine':
    print('Correct!')
    score += 1
else:
    print('Incorrect!')

answer = input('What number is next in the series: 172, 84, 40, 18, …?  ')
if answer.lower() == '7' or 'seven':
    print('Correct!')
    score += 1
else:
    print('Incorrect!')

answer = input('How many stamps are in a dozen?  ')
if answer.lower() == '12' or 'twelve':
    print('Correct!')
    score += 1
else:
    print('Incorrect!')


print('You got ' + str(score) + ' correct answer!')
print('You got ' + str((score / 4) * 100) + '%.')
